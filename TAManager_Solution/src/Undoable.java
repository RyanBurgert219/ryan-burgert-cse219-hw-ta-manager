// //Created by Ryan on 3/12/17.
// // public class Undoable//A single undoable action. Does stuff upon construction.
// // {
// //     Undoable next;
// //     Undoable prev;
// //
// //
// //     private Runnable toDo;
// //     private Runnable toUndo;
// //     private boolean trueIfDone_falseIfUndone=false;//Used to keep track of if how many times we do something. I'm not allowed to run the 'do' function multiple times without first calling 'undo'. It is false initially because right now, as of the evaluation of THIS LINE OF CODE, toDo hasn't been done yet.
// //
// //     public Undoable(Runnable toDo,Runnable toUndo)
// //     {
// //         this.toDo=toDo;
// //         this.toDo=toUndo;
// //     }
// //
// //     public Undoable Do()
// //     {
// //         assert !trueIfDone_falseIfUndone;//Assert undone
// //         toDo.run();
// //         trueIfDone_falseIfUndone=true;
// //     }
// //     public Undoable Undo()
// //     {
// //         assert trueIfDone_falseIfUndone;//Assert done
// //         toUndo.run();
// //         trueIfDone_falseIfUndone=false;
// //     }
// // }
// class tam.UndoRedoCoordinator
// {
//     class Undoable
//     {
//         Runnable toDo,toUndo;
//         boolean done=false;//True if toDoo has been run, false if either toDoo has NOT been run OR toUndo has been run. If done is false and toUndo is run, we should get an assertion error. For debugging assertion purposes. Isn't necessary if this class works properly.
//         Undoable prev=null,next=null;//Undoable acts like a singly linked list. By default the far right-end of the chain should be null, as should the far left end. This is the far right end, AKA …ext.next.next.nex…
//     }
//     Undoable cursor=new Undoable();
//     public void doSomething(Runnable toDo,Runnable toUndo)//It must be possible for that something to be 'undone', as you can see from the required arguments.
//     {
//         cursor.toDo=toDo;
//         cursor.toUndo=toUndo;
//
//         //run todo
//         assert !cursor.done;
//         cursor.toDo.run();//≣toDo.run()
//         cursor.done=true;
//
//         cursor.next=new Undoable();//This is the only place where Undoables are created… We're preparing a new action…
//         cursor.next.prev=cursor;//Initially this is null; at the far left end. AKA …rev.prev.prev.pre… == null
//         cursor.next.toDo=toDo;
//         cursor.next.toUndo=toUndo;
//     }
//     public void undo()
//     {
//         assert cursor.done||cursor.prev!=null;
//         assert cursor.prev==null||!cursor.prev.done;// ∃ next ⟹ next is undone
//         cursor.toUndo.run();
//         cursor.done=false;
//         cursor=cursor.prev;
//     }
//     public void redo()
//     {
//         assert !cursor.done||cursor.next!=null;
//         assert cursor.next==null||!cursor.next.done;// ∃ next ⟹ next is undone
//         cursor.toDo.run();
//         cursor.done=true;
//         cursor=cursor.next;//⟵ ORDER MATTERS FOR THIS LINE: Must be last
//     }
// }