//BEHOLD: THIS FILE IS WHERE ALL THE BUTTONS AND STUFF COME FROM. THE ACTUAL JAVAFX BOILERPLATE STUFF. ~ryan
package tam.workspace;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;

import java.util.ArrayList;
import java.util.HashMap;

import javafx.collections.FXCollections;
import javafx.scene.control.*;
import tam.TAManagerApp;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import tam.TAManagerProp;
import tam.style.TAStyle;
import tam.data.TAData;
import tam.data.TeachingAssistant;

import static tam.workspace.TAController.say;
/**
 * This class serves as the workspace component for the TA Manager
 * application. It provides all the user interface controls in
 * the workspace area.
 *
 * @author Richard McKenna
 */
public class TAWorkspace extends AppWorkspaceComponent
{
    //region combobox stuff
    public ObservableList<String> timeSelection=FXCollections.observableArrayList("12:00am",
    /*  Don't delete me  */                                                                          "1:00am",
    /*  Don't delete me  */                                                                          "2:00am",
    /*  Don't delete me  */                                                                          "3:00am",
    /*  Don't delete me  */                                                                          "4:00am",
    /*  Don't delete me  */                                                                          "5:00am",
    /*  Don't delete me  */                                                                          "6:00am",
    /*  Don't delete me  */                                                                          "7:00am",
    /*  Don't delete me  */                                                                          "8:00am",
    /*  Don't delete me  */                                                                          "9:00am",
    /*  Don't delete me  */                                                                          "10:00am",
    /*  Don't delete me  */                                                                          "11:00am",
    /*  Don't delete me  */                                                                          "12:00pm",
    /*  Don't delete me  */                                                                          "1:00pm",
    /*  Don't delete me  */                                                                          "2:00pm",
    /*  Don't delete me  */                                                                          "3:00pm",
    /*  Don't delete me  */                                                                          "4:00pm",
    /*  Don't delete me  */                                                                          "5:00pm",
    /*  Don't delete me  */                                                                          "6:00pm",
    /*  Don't delete me  */                                                                          "7:00pm",
    /*  Don't delete me  */                                                                          "8:00pm",
    /*  Don't delete me  */                                                                          "9:00pm",
    /*  Don't delete me  */                                                                          "10:00pm",
    /*  Don't delete me  */                                                                          "11:00pm"
    /*  Don't delete me  */);
    ComboBox<String> ryanStartTimesComboBox=new ComboBox<>(timeSelection);
    ComboBox<String> ryanEndTimesComboBox=new ComboBox<>(timeSelection);
    //endregion
    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    TAManagerApp app;
    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    TAController controller;
    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    // FOR THE HEADER ON THE LEFT
    HBox tasHeaderBox;
    public HBox ryansSetTaTimesHeaderBox;
    Label tasHeaderLabel;
    public Label ryansSetTaTimesHeaderLabel;
    public Label ryansComboBoxStartLabel;
    public Label ryansComboBoxEndLabel;
    GridPane ryansSetTimesComboBoxesGridPane;
    // FOR THE TA TABLE
    TableView<TeachingAssistant> taTable;
    TableColumn<TeachingAssistant,String> nameColumn;
    TableColumn<TeachingAssistant,String> emailColumn;
    // THE TA INPUT
    HBox addBox;
    TextField nameTextField;
    TextField emailTextField;
    Button addButton;
    Button clearButton;
    // THE HEADER ON THE RIGHT
    HBox officeHoursHeaderBox;
    Label officeHoursHeaderLabel;
    // THE OFFICE HOURS GRID
    GridPane officeHoursGridPane;
    HashMap<String,Pane> officeHoursGridTimeHeaderPanes;
    HashMap<String,Label> officeHoursGridTimeHeaderLabels;
    HashMap<String,Pane> officeHoursGridDayHeaderPanes;
    HashMap<String,Label> officeHoursGridDayHeaderLabels;
    HashMap<String,Pane> officeHoursGridTimeCellPanes;
    HashMap<String,Label> officeHoursGridTimeCellLabels;
    HashMap<String,Pane> officeHoursGridTACellPanes;
    HashMap<String,Label> officeHoursGridTACellLabels;
    /**
     * The constructor initializes the user interface, except for
     * the full office hours grid, since it doesn't yet know what
     * the hours will be until _a_ file is loaded or _a_ new one is created.
     */
    public TAWorkspace(TAManagerApp initApp)
    {
        /*@formatter:off*/

        // KEEP THIS FOR LATER
        app=initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props=PropertiesManager.getPropertiesManager();

        // INIT THE HEADER ON THE LEFT
        tasHeaderBox=new HBox();
        ryansSetTaTimesHeaderBox=new HBox();
        String tasHeaderText=props.getProperty(TAManagerProp.TAS_HEADER_TEXT.toString());
        tasHeaderLabel=new Label(tasHeaderText);
        ryansSetTaTimesHeaderLabel=new Label(props.getProperty(TAManagerProp.RYANS_TA_TIME_SELECTOR_COMBOBOX_HEADER_LABEL_TEXT.toString()));
        ryansComboBoxStartLabel=new Label(props.getProperty(TAManagerProp.RYANS_TA_START_TIME_COMBOBOX_LABEL_TEXT.toString()));
        ryansComboBoxEndLabel=new Label(props.getProperty(TAManagerProp.RYANS_TA_END_TIME_COMBOBOX_LABEL_TEXT.toString()));
        tasHeaderBox.getChildren().add(tasHeaderLabel);
        ryansSetTaTimesHeaderBox.getChildren().add(ryansSetTaTimesHeaderLabel);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        taTable=new TableView();
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TAData data=(TAData)app.getDataComponent();
        ObservableList<TeachingAssistant>tableData=data.getTeachingAssistants();
        taTable.setItems(tableData);
        String nameColumnText=props.getProperty(TAManagerProp.NAME_COLUMN_TEXT.toString());
        String emailColumnText=props.getProperty(TAManagerProp.EMAIL_COLUMN_TEXT.toString());
        nameColumn=new TableColumn(nameColumnText);
        emailColumn=new TableColumn(emailColumnText);
        nameColumn.setCellValueFactory(new PropertyValueFactory<TeachingAssistant,String>("name"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<TeachingAssistant,String>("email"));
        taTable.getColumns().add(nameColumn);
        taTable.getColumns().add(emailColumn);

        // ADD BOX FOR ADDING A TA
        String namePromptText=props.getProperty(TAManagerProp.NAME_PROMPT_TEXT.toString());
        String emailPromptText=props.getProperty(TAManagerProp.EMAIL_PROMPT_TEXT.toString());
        String addButtonText=props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString());
        String clearButtonText=props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString());
        nameTextField=new TextField();
        emailTextField=new TextField();
        nameTextField.setPromptText(namePromptText);
        emailTextField.setPromptText(emailPromptText);
        addButton=new Button(addButtonText);
        clearButton=new Button(clearButtonText);
        addBox=new HBox();
        nameTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        emailTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        addButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        clearButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        clearButton.prefHeightProperty().bind(addBox.heightProperty());//Ryan: Make sure the clear button has the same height. By default, it does not.
        addBox.getChildren().add(nameTextField);
        addBox.getChildren().add(emailTextField);
        addBox.getChildren().add(addButton);
        addBox.getChildren().add(clearButton);

        // INIT THE HEADER ON THE RIGHT
        officeHoursHeaderBox=new HBox();
        String officeHoursGridText=props.getProperty(TAManagerProp.OFFICE_HOURS_SUBHEADER.toString());
        officeHoursHeaderLabel=new Label(officeHoursGridText);
        officeHoursHeaderBox.getChildren().add(officeHoursHeaderLabel);

        // THESE WILL STORE PANES AND LABELS FOR OUR OFFICE HOURS GRID
        officeHoursGridPane=new GridPane();//The right pane of the two big ones
        officeHoursGridTimeHeaderPanes=new HashMap<>();
        officeHoursGridTimeHeaderLabels=new HashMap<>();
        officeHoursGridDayHeaderPanes=new HashMap<>();
        officeHoursGridDayHeaderLabels=new HashMap<>();
        officeHoursGridTimeCellPanes=new HashMap<>();
        officeHoursGridTimeCellLabels=new HashMap<>();
        officeHoursGridTACellPanes=new HashMap<>();
        officeHoursGridTACellLabels=new HashMap<>();

        // ORGANIZE THE LEFT AND RIGHT PANES
        VBox leftPane=new VBox();
        leftPane.getChildren().add(tasHeaderBox);
        leftPane.getChildren().add(taTable);
        leftPane.getChildren().add(addBox);
        VBox rightPane=new VBox();
        rightPane.getChildren().add(officeHoursHeaderBox);
        rightPane.getChildren().add(officeHoursGridPane);

        //region Ryan Stuff tests       ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
        VBox superDuperRightPane=new VBox();//Right of the Right pane
        superDuperRightPane.getChildren().add(ryansSetTaTimesHeaderLabel);
            HBox ComboBoxes=new HBox();


            ryanStartTimesComboBox.setPromptText("Start Hour");
            ryanEndTimesComboBox.setPromptText("End Hour");
            ComboBoxes.getChildren().add(new VBox(ryansComboBoxStartLabel,ryanStartTimesComboBox));
            ComboBoxes.getChildren().add(new VBox(ryansComboBoxEndLabel,ryanEndTimesComboBox));



        superDuperRightPane.getChildren().add(ComboBoxes);


        // ryansSetTimesComboBoxesGridPane=new GridPane();
        // superDuperRightPane.getChildren().add(ryanStartTimesComboBox);
        // ryansSetTimesComboBoxesGridPane.addRow(0,ryansComboBoxStartLabel,ryansComboBoxEndLabel);
        // ryansSetTimesComboBoxesGridPane.addRow(1,ryanStartTimesComboBox,ryanEndTimesComboBox);
        // superDuperRightPane.getChildren().add(ryansSetTimesComboBoxesGridPane);
        //endregion

        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane sPane=new SplitPane(leftPane,new ScrollPane(rightPane),superDuperRightPane);
        workspace=new BorderPane();

        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane)workspace).setCenter(sPane);

        // MAKE SURE THE TABLE EXTENDS DOWN FAR ENOUGH

        taTable.prefHeightProperty().bind(workspace.heightProperty().multiply(1.9));
        // NOW LET'S SETUP THE EVENT HANDLING

        controller=new TAController(app);
        // CONTROLS FOR ADDING TAs


        ryanStartTimesComboBox.getSelectionModel().selectedItemProperty().addListener((ⵁ,oldTime,newTime)->controller.handleChangedStartTime(ryanStartTimesComboBox,oldTime,newTime));
        ryanEndTimesComboBox.getSelectionModel().selectedItemProperty().addListener((ⵁ,oldTime,newTime)->controller.handleChangedEndTime(ryanEndTimesComboBox,oldTime,newTime));
        nameTextField.setOnAction(e->controller.handleAddⳆUpdateTA());
        emailTextField.setOnAction(e->controller.handleAddⳆUpdateTA());
        addButton.setOnAction(e->controller.handleAddⳆUpdateTA());
        clearButton.setOnAction(e->controller.handleClearButton());
        taTable.setFocusTraversable(true);
        taTable.setOnKeyPressed(e->controller.handleKeyPress(e.getCode()));
        taTable.getSelectionModel().selectedItemProperty().addListener((idkwhatthisdoes,oldSelected,newSelected)->controller.handleTASelected());
        emailTextField.textProperty().addListener((idkwhatthisdoes,oldText,newText)->controller.handleEmailTextFieldChanged());
        nameTextField.textProperty().addListener((idkwhatthisdoes,oldText,newText)->controller.handleNameTextFieldChanged());
        workspace.setOnKeyPressed(e->controller.handleKeyPressTheRyanWay(e));

        /*@formatter:on*/
    }
    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    public HBox getTAsHeaderBox()
    {
        return tasHeaderBox;
    }
    public Label getTAsHeaderLabel()
    {
        return tasHeaderLabel;
    }
    public TableView getTATable()
    {
        return taTable;
    }
    public HBox getAddBox()
    {
        return addBox;
    }
    public TextField getNameTextField()
    {
        return nameTextField;
    }
    public TextField getEmailTextField()
    {
        return emailTextField;
    }
    public Button getAddButton()
    {
        return addButton;
    }
    public Button getClearButton()
    {
        return clearButton;
    }
    public HBox getOfficeHoursSubheaderBox()
    {
        return officeHoursHeaderBox;
    }
    public Label getOfficeHoursSubheaderLabel()
    {
        return officeHoursHeaderLabel;
    }
    public GridPane getOfficeHoursGridPane()
    {
        return officeHoursGridPane;
    }
    public HashMap<String,Pane> getOfficeHoursGridTimeHeaderPanes()
    {
        return officeHoursGridTimeHeaderPanes;
    }
    public HashMap<String,Label> getOfficeHoursGridTimeHeaderLabels()
    {
        return officeHoursGridTimeHeaderLabels;
    }
    public HashMap<String,Pane> getOfficeHoursGridDayHeaderPanes()
    {
        return officeHoursGridDayHeaderPanes;
    }
    public HashMap<String,Label> getOfficeHoursGridDayHeaderLabels()
    {
        return officeHoursGridDayHeaderLabels;
    }
    public HashMap<String,Pane> getOfficeHoursGridTimeCellPanes()
    {
        return officeHoursGridTimeCellPanes;
    }
    public HashMap<String,Label> getOfficeHoursGridTimeCellLabels()
    {
        return officeHoursGridTimeCellLabels;
    }
    public HashMap<String,Pane> getOfficeHoursGridTACellPanes()
    {
        return officeHoursGridTACellPanes;
    }
    public HashMap<String,Label> getOfficeHoursGridTACellLabels()
    {
        return officeHoursGridTACellLabels;
    }
    public String getCellKey(Pane testPane)
    {
        for(String key : officeHoursGridTACellLabels.keySet())
        {
            if(officeHoursGridTACellPanes.get(key)==testPane)
            {
                return key;
            }
        }
        return null;
    }
    public Label getTACellLabel(String cellKey)
    {
        return officeHoursGridTACellLabels.get(cellKey);
    }
    public Pane getTACellPane(String cellPane)
    {
        return officeHoursGridTACellPanes.get(cellPane);
    }
    public String buildCellKey(int col,int row)
    {
        return ""+col+"_"+row;
    }
    public String buildCellText(int militaryHour,String minutes)
    {
        int m=Integer.parseInt(minutes);
        if(militaryHour==24&&m==0)//Ryan overrides
        {
            return "12:00am";
        }
        if(militaryHour==0&&m==0)//Ryan overrides
        {
            return "12:00am";
        }
        if(militaryHour==0&&m==30)//Ryan overrides
        {
            return "12:30am";
        }
        // FIRST THE START AND END CELLS
        //todo remember where i am
        int hour=militaryHour;
        if(hour>12)
        {
            hour-=12;
        }
        String cellText=""+hour+":"+minutes;
        if(militaryHour<12)
        {
            cellText+="am";
        }
        else
        {
            cellText+="pm";
        }
        return cellText;
    }
    @Override
    public void resetWorkspace()
    {
        // CLEAR OUT THE GRID PANE
        officeHoursGridPane.getChildren().clear();
        // AND THEN ALL THE GRID PANES AND LABELS
        officeHoursGridTimeHeaderPanes.clear();
        officeHoursGridTimeHeaderLabels.clear();
        officeHoursGridDayHeaderPanes.clear();
        officeHoursGridDayHeaderLabels.clear();
        officeHoursGridTimeCellPanes.clear();
        officeHoursGridTimeCellLabels.clear();
        officeHoursGridTACellPanes.clear();
        officeHoursGridTACellLabels.clear();
    }
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent)
    {
        TAData taData=(TAData)dataComponent;
        reloadOfficeHoursGrid(taData);
    }
    //region Ryanstuffs
    public void setStartAndEndHours(int startHour,int endHour)
    {
        //region Assertions about input hours
        assert officeHourTimesAreValid(startHour,endHour,true);
        // endregion
        final int sh=startHour, eh=endHour;
        final String oldState=controller.getGridStateIncludingSize();
        app.urc.Do(()->
                   {
                       System.out.println(System.currentTimeMillis());
                       String state=controller.getGridStateExceptShape();
                       String stateWithSize=controller.getGridStateIncludingSize();
                       _setStartAndEndHours(sh,eh);
                       if(!controller.setGridStateExceptShape(state))//Tas were deleted
                       {
                           if(!controller.yesNoDialog(PropertiesManager.getPropertiesManager().getProperty("ALERTDIALOGTEXT")))
                           {
                               say("Ok reverting");
                               controller.setGridStateIncludingSize(stateWithSize);
                           }
                           else
                           {
                               say("ok proceeding");
                           }
                       }
                   },()->
                   {
                       controller.setGridStateIncludingSize(oldState);
                   });
    }
    public void _setStartAndEndHours(int startHour,int endHour)
    {
        //region Assertions about input hours
        assert officeHourTimesAreValid(startHour,endHour,false);
        // endregion
        TAData data=(TAData)app.getDataComponent();
        String state=controller.getGridStateExceptShape();
        data.endHour=endHour;
        data.startHour=startHour;
        data.getGridHeaders();
        reloadOfficeHoursGrid(data);
        // controller.clearGridStateExceptShape();
        controller.setGridStateExceptShape(state);
    }
    public boolean officeHourTimesAreValid(int startHour,int endHour,boolean showDialog)
    {
        if(!(startHour<endHour))
        {
            say("error not start Hour less than end Hour");
            if(showDialog)
            {
                controller.showDialog("Invalid Start/End Times","Cannot change TA time bounds. Please make sure that the start time you selected is BEFORE the end time.");
            }
            return false;
        }
        if(!(startHour>=TAData.MIN_START_HOUR))
        {
            say("error not start Hour more than 0");
            return false;
        }
        if(!(endHour<=TAData.MAX_END_HOUR))
        {
            say("error not end hour less than 25");
            return false;//? might wanna double check that its not 23
        }
        return true;
    }
    //endregion
    public void reloadOfficeHoursGrid(TAData dataComponent)
    {
        officeHoursGridPane.getChildren().clear();
        ArrayList<String> gridHeaders=dataComponent.getGridHeaders();
        // ADD THE TIME HEADERS
        for(int i=0;i<2;i++)
        {
            addCellToGrid(dataComponent,officeHoursGridTimeHeaderPanes,officeHoursGridTimeHeaderLabels,i,0);
            dataComponent.getCellTextProperty(i,0).set(gridHeaders.get(i));
        }
        // THEN THE DAY OF WEEK HEADERS
        for(int i=2;i<7;i++)
        {
            addCellToGrid(dataComponent,officeHoursGridDayHeaderPanes,officeHoursGridDayHeaderLabels,i,0);
            dataComponent.getCellTextProperty(i,0).set(gridHeaders.get(i));
        }
        // THEN THE TIME AND TA CELLS
        int row=1;
        for(int i=dataComponent.getStartHour();i<dataComponent.getEndHour();i++)
        {
            // START TIME COLUMN
            int col=0;
            addCellToGrid(dataComponent,officeHoursGridTimeCellPanes,officeHoursGridTimeCellLabels,col,row);
            dataComponent.getCellTextProperty(col,row).set(buildCellText(i,"00"));
            addCellToGrid(dataComponent,officeHoursGridTimeCellPanes,officeHoursGridTimeCellLabels,col,row+1);
            dataComponent.getCellTextProperty(col,row+1).set(buildCellText(i,"30"));
            // END TIME COLUMN
            col++;
            int endHour=i;
            addCellToGrid(dataComponent,officeHoursGridTimeCellPanes,officeHoursGridTimeCellLabels,col,row);
            dataComponent.getCellTextProperty(col,row).set(buildCellText(endHour,"30"));
            addCellToGrid(dataComponent,officeHoursGridTimeCellPanes,officeHoursGridTimeCellLabels,col,row+1);
            dataComponent.getCellTextProperty(col,row+1).set(buildCellText(endHour+1,"00"));
            col++;
            // AND NOW ALL THE TA TOGGLE CELLS
            while(col<7)
            {
                addCellToGrid(dataComponent,officeHoursGridTACellPanes,officeHoursGridTACellLabels,col,row);
                addCellToGrid(dataComponent,officeHoursGridTACellPanes,officeHoursGridTACellLabels,col,row+1);
                col++;
            }
            row+=2;
        }
        // CONTROLS FOR TOGGLING TA OFFICE HOURS
        for(Pane p : officeHoursGridTACellPanes.values())
        {
            p.setFocusTraversable(true);
            p.setOnKeyPressed(e->controller.handleKeyPress(e.getCode()));
            p.setOnMouseClicked(e->controller.handleCellToggle((Pane)e.getSource()));
            p.setOnMouseExited(e->controller.handleGridCellMouseExited((Pane)e.getSource()));
            p.setOnMouseEntered(e->controller.handleGridCellMouseEntered((Pane)e.getSource()));
        }
        // AND MAKE SURE ALL THE COMPONENTS HAVE THE PROPER STYLE
        TAStyle taStyle=(TAStyle)app.getStyleComponent();
        taStyle.initOfficeHoursGridStyle();
    }
    public void addCellToGrid(TAData dataComponent,HashMap<String,Pane> panes,HashMap<String,Label> labels,int col,int row)
    {
        // MAKE THE LABEL IN A PANE
        Label cellLabel=new Label("");
        HBox cellPane=new HBox();
        cellPane.setAlignment(Pos.CENTER);
        cellPane.getChildren().add(cellLabel);
        // BUILD A KEY TO EASILY UNIQUELY IDENTIFY THE CELL
        String cellKey=dataComponent.getCellKey(col,row);
        cellPane.setId(cellKey);
        cellLabel.setId(cellKey);
        // NOW PUT THE CELL IN THE WORKSPACE GRID
        officeHoursGridPane.add(cellPane,col,row);
        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
        panes.put(cellKey,cellPane);
        labels.put(cellKey,cellLabel);
        // AND FINALLY, GIVE THE TEXT PROPERTY TO THE DATA MANAGER
        // SO IT CAN MANAGE ALL CHANGES
        dataComponent.setCellProperty(col,row,cellLabel.textProperty());
    }
    // public void removeCellFromGrid(TAData dataComponent,HashMap<String,Pane> panes,HashMap<String,Label> labels,int col,int row)
    // {
    //     BUILD A KEY TO EASILY UNIQUELY IDENTIFY THE CELL
    // String cellKey=dataComponent.getCellKey(col,row);
    // NOW PUT THE CELL IN THE WORKSPACE GRID
    // officeHoursGridPane.getChildren().remove(officeHoursGridTACellPanes.get(cellKey));
    // panes.remove(cellKey);
    // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
    // labels.remove(cellKey);
    // AND FINALLY, GIVE THE TEXT PROPERTY TO THE DATA MANAGER
    // SO IT CAN MANAGE ALL CHANGES
    // dataComponent.setCellProperty(col,row,cellLabel.textProperty());
    // }
}
