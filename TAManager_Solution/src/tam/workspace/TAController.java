package tam.workspace;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.TAManagerProp;
import tam.data.TAData;
import tam.data.TeachingAssistant;

import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Pattern;

import static tam.TAManagerProp.*;
import static tam.style.TAStyle.*;
/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class TAController
{
    //region Ryan Stuff
    //region r Class Stuff
    public void sleep(double n)
    {
        try
        {
            Thread.sleep((long)(n*1000));
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }
    public static String replaceAll(String Original,String OldSnippet,String NewSnippet)
    {
        //noinspection StatementWithEmptyBody
        return Original.replaceAll(Pattern.quote(OldSnippet),NewSnippet);
    }
    public static void attemptShellCommandIgnoreExceptions(String command)
    {
        try
        {
            Runtime.getRuntime().exec(command);
        }
        catch(Exception ignored)
        {
        }
    }
    public void handleChangedStartTime(ComboBox combobox,String oldTime,String newTime)
    {
        final int oldEndHour=getTAData().endHour;
        final int oldStartHour=getTAData().startHour;
        final int newEndHour=getTAData().endHour;
        final int newStartHour=getWorkspace().timeSelection.indexOf(newTime);
        say("Changed start time from "+oldTime+" to "+newTime+" AKA dot dot dot from "+oldEndHour+" to "+newEndHour);
        getWorkspace().setStartAndEndHours(newStartHour,newEndHour);//This method DOES take into account doing and undoing!!
    }
    public void handleChangedEndTime(ComboBox combobox,String oldTime,String newTime)
    {
        final int oldEndHour=getTAData().endHour;
        final int oldStartHour=getTAData().startHour;
        final int newEndHour=getWorkspace().timeSelection.indexOf(newTime);
        final int newStartHour=getTAData().startHour;
        say("Changed end time from "+oldTime+" to "+newTime+" AKA dot dot dot from "+oldStartHour+" to "+newEndHour);
        getWorkspace().setStartAndEndHours(newStartHour,newEndHour);//This method DOES take into account doing and undoing!!

    }
    //region Audio: Text to Speech (only on Macs)
    public enum voices
    {
        //Used for the 'say' method. All possible voices on the MacOsx say command.
        Samantha,Bad,Bahh,Bells,Boing,Bubbles,Cellos,Deranged,Good,Hysterical,Pipe,Trinoids,Whisper,Zarvox,Agnes,Kathy,Princess,Vicki,Victoria,Alex,Bruce,Fred,Junior,Ralph,Albert
    }
    public static void say(String message,voices voice)
    {
        //I've only tested this method on my Mac. I don't think it will work on windows as it relies on the Mac's terminal-based text-to-speech.
        //It will invoke _a_ text-to-speech method on the Mac's bash (or terminal or shell idk what to call it).
        attemptShellCommandIgnoreExceptions("say -v "+voice+" "+message);
    }
    public static void say(String message)
    {
        voices defaultVoice=voices.Samantha;//This can be changed based on how I feel today.
        say(message,defaultVoice);
    }
    //endregion
    //endregion
    //region misc
    public TAWorkspace getWorkspace()
    {
        return (TAWorkspace)app.getWorkspaceComponent();
    }
    public TeachingAssistant getSelectedTA()//Returns null if none selected
    {
        return (TeachingAssistant)getWorkspace().getTATable().getSelectionModel().getSelectedItem();
    }
    public TAData getTAData()
    {
        return (TAData)app.getDataComponent();
    }
    public void showDialog(String title,String message)//By Ryan Burgert
    {
        AppMessageDialogSingleton.getSingleton().show(title,message);
    }
    public boolean yesNoDialog(String message)//By Ryan Burgert
    {
        Alert ⵁ = new Alert(Alert.AlertType.CONFIRMATION,message,ButtonType.YES,ButtonType.NO,ButtonType.CANCEL);
        ⵁ.showAndWait();
        return (ⵁ.getResult() == ButtonType.YES);
    }
    public String getXMLproperty(Object propTitle)
    {
        //propTitle should come as an enum from TAManagerProp.java
        //Accesses stuff from app_properties.xml
        return PropertiesManager.getPropertiesManager().getProperty(propTitle);
    }
    public void showDialog(TAManagerProp title,TAManagerProp message)//By Ryan Burgert
    {
        showDialog(getXMLproperty(title),getXMLproperty(message));
    }
    public static boolean isValidEmail(String s)
    {
        //CODE FROM: https://www.mkyong.com/regular-expressions/how-to-validate-email-address-with-regular-expression/
//⁠⁠⁠⁠⁠                      ⎧                                                                                          ⎫
//⁠⁠⁠⁠⁠                      ⎪                   ⎧                 ⎫               ⎧               ⎫ ⎧               ⎫  ⎪
//⁠⁠⁠⁠⁠                      ⎪  ⎧              ⎫ ⎪   ⎧           ⎫ ⎪  ⎧          ⎫ ⎪   ⎧         ⎫ ⎪ ⎪   ⎧      ⎫⎧  ⎫⎪  ⎪
        return s.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
//⁠⁠⁠⁠⁠                      ⎪  ⎩              ⎭ ⎪   ⎩           ⎭ ⎪  ⎩          ⎭ ⎪   ⎩         ⎭ ⎪ ⎪   ⎩      ⎭⎩  ⎭⎪  ⎪
//⁠⁠⁠⁠⁠                      ⎪                   ⎩                 ⎭               ⎩               ⎭ ⎩               ⎭  ⎪
//⁠⁠⁠⁠⁠                      ⎩                                                                                          ⎭
    }
    //endregion
    //region ［timeDayToId，IDtoStartTime，IDtoDay］
    //       2_1    3_1    4_1    5_1    6_1
    //       2_2    3_2    4_2    5_2    6_2
    //       2_3    3_3    4_3    5_3    6_3
    //       2_4    3_4    4_4    5_4    6_4
    //       2_5    3_5    4_5    5_5    6_5
    //       2_6    3_6    4_6    5_6    6_6
    //       2_7    3_7    4_7    5_7    6_7
    //       2_8    3_8    4_8    5_8    6_8
    //       2_9    3_9    4_9    5_9    6_9
    //       2_10   3_10   4_10   5_10   6_10
    //       2_11   3_11   4_11   5_11   6_11
    //       2_12   3_12   4_12   5_12   6_12
    //       2_13   3_13   4_13   5_13   6_13
    //       2_14   3_14   4_14   5_14   6_14
    //       2_15   3_15   4_15   5_15   6_15
    //       2_16   3_16   4_16   5_16   6_16
    //       2_17   3_17   4_17   5_17   6_17
    //       2_18   3_18   4_18   5_18   6_18
    //       2_19   3_19   4_19   5_19   6_19
    //       2_20   3_20   4_20   5_20   6_20
    //       2_21   3_21   4_21   5_21   6_21
    //       2_22   3_22   4_22   5_22   6_22
    public String timeDayToID(String startTime,String day)//Confirmed to work correctly. Tested all grid values
    {
        /*@formatter:off*/
        //EXAMPLE: timeDayToID("MONDAY","10:00am") ⟶ "4_3"
        //Time/Day MUST match the strings that appear on the column/row labels of the GUI
        //region Match the day to the first int of the ID:
        String x="";
        String m="MONDAY",tu="TUESDAY",w="WEDNESDAY",th="THURSDAY",f="FRIDAY";
        assert (m+tu+w+th+f).contains(day);//Sloppy but who cares
        if(day.equals(m))
            x+=2;
        if(day.equals(tu))
            x+=3;
        if(day.equals(w))
            x+=4;
        if(day.equals(th))
            x+=5;
        if(day.equals(f))
            x+=6;
        assert !x.equals("");

        // String x=""+getTAData().gridHeaders.indexOf(day); ⟵ A replacement for the above arbitrary code?

        //endregion
        //region Match the day number to the first int of the ID:


        for(Label label : getWorkspace().getOfficeHoursGridTimeCellLabels().values())
            if(label.getId().charAt(0)=='0')//Only concerned with the start time. End time is if charAt(2)=='1'. EX: 5_21  ⟵ 0th char is 5 ∴  this is not _a_ start time
                if(label.getText().equals(startTime))
                    x+=label.getId().substring(label.getId().indexOf('_'));





        return x;
        /*@formatter:on*/
    }
    public String IDtoStartTime(String ID)
    {
//⁠⁠⁠⁠⁠                                                                  ⎧                                 ⎫
//⁠⁠⁠⁠⁠                                                                  ⎪                ⎧               ⎫⎪
//⁠⁠⁠⁠⁠                         ⎧⎫                                 ⎧⎫    ⎪                ⎪          ⎧   ⎫⎪⎪        ⎧⎫
        return getWorkspace().getOfficeHoursGridTimeCellLabels().get("0"+ID.substring(ID.indexOf('_'))).getText();
//⁠⁠⁠⁠⁠                         ⎩⎭                                 ⎩⎭    ⎪                ⎪          ⎩   ⎭⎪⎪        ⎩⎭
//⁠⁠⁠⁠⁠                                                                  ⎪                ⎩               ⎭⎪
//⁠⁠⁠⁠⁠                                                                  ⎩                                 ⎭
    }
    public String IDtoDay(String ID)
    {
//⁠⁠⁠⁠⁠                                                                   ⎧                                    ⎫
//⁠⁠⁠⁠⁠                                                                   ⎪            ⎧                 ⎫     ⎪
//⁠⁠⁠⁠⁠                         ⎧⎫                                  ⎧⎫    ⎪            ⎪            ⎧   ⎫⎪     ⎪        ⎧⎫
        return getWorkspace().getOfficeHoursGridDayHeaderLabels().get(ID.substring(0,ID.indexOf('_'))+"_0").getText();
//⁠⁠⁠⁠⁠                         ⎩⎭                                  ⎩⎭    ⎪            ⎪            ⎩   ⎭⎪     ⎪        ⎩⎭
//⁠⁠⁠⁠⁠                                                                   ⎪            ⎩                 ⎭     ⎪
//⁠⁠⁠⁠⁠                                                                   ⎩                                    ⎭
    }
    //endregion
    //region Gridstate saving except size
    private char _a_='⏣';//;
    private char _b_='◌';//,
    public String getGridStateIncludingSize()//Returns the string needed to restore the TA with their office hours.
    {
        String out="";//Format: (first character is literally the value of the start hour, second chatacter is literally the value of the end hour)time,day,text;time,day,text;… ⟵ Is useful because its independent of row/column numbers.
        out+=(char)(getTAData().startHour);
        out+=(char)(getTAData().endHour);
        out+=getGridStateExceptShape();
        return out;
    }
    public boolean setGridStateIncludingSize(String state)//IS NOT UNDOABLE!!!
    {//IS NOT UNDOABLE!!!
        int starthour=state.charAt(0);//IS NOT UNDOABLE!!!
        int endhour=state.charAt(1);//IS NOT UNDOABLE!!!
        getWorkspace()._setStartAndEndHours(starthour,endhour);//IS NOT UNDOABLE!!!
        return setGridStateExceptShape(state.substring(2));//IS NOT UNDOABLE!!!
    }
    public String getGridStateExceptShape()//Returns the string needed to restore the TA with their office hours.
    {
        String out="";//Format: time,day,text;time,day,text;… ⟵ Is useful because its independent of row/column numbers.
        for(Label label : getWorkspace().getOfficeHoursGridTACellLabels().values())
        {
            String id=label.getId();
            out+=_a_+IDtoStartTime(id)+_b_+IDtoDay(id)+_b_;
            // if(timeDayToID(IDtoStartTime(id),IDtoDay(id)).equals(id))
            // {
            out+=label.getText();//;time,day,text
            // }
        }
        System.out.println(out);
        return out.substring(1);//get rid of the first semicolon
    }
    public boolean setGridStateExceptShape(String state)//Returns the string needed to restore the TA with their office hours.
    {
        boolean flag=true;
        clearGridStateExceptShape();//Otherwise we get junked up with random crap. We need it to erase office hours if the hours shrink.
        HashMap<String,Label> labels=getWorkspace().getOfficeHoursGridTACellLabels();
        // say("blah blah blah blah blah blah blah but but but  "+getWorkspace().getOfficeHoursGridTACellPanes().keySet().containsAll(getWorkspace().getOfficeHoursGridTACellPanes().keySet());
        // say("blah blah blah blah blah blah blah but but but  "+getWorkspace().getOfficeHoursGridTACellPanes().keySet().containsAll(getWorkspace().getOfficeHoursGridTACellLabels().keySet()));
        // say("blah blah blah blah blah blah blah but but but aaaaaaaaaaaaa aaaaaaaaa aaaaaaaaa aaaaaaaaa aaaaaaaa"+getWorkspace().getOfficeHoursGridTACellLabels().keySet().containsAll(getWorkspace().getOfficeHoursGridTACellPanes().keySet()));
        // System.out.println("――――――――――――――――――――――――――――――― ―――――――――――lkjoiho――――――――――――――――――");
        // for(Object x : getWorkspace().getOfficeHoursGridTACellPanes().keySet().toArray())
        // {
        //     System.out.println(x);
        // }
        // System.out.println("―――――――――――uytfugugf―――――――――――――――――――――――――――――――――――――――――――――――――");
        // for(Object x : getWorkspace().getOfficeHoursGridTACellLabels().keySet().toArray())
        // {
        //     System.out.println(x);
        // }
        // System.out.println("―――――――――――uytfugugf――――――――――oijj―――――――――――――――――――――――khjio――――――――――――――――");
        for(String x : state.split(""+_a_))
        {
            String[] y=x.split(""+_b_);
            StringProperty tp;
            try
            {
                String id=timeDayToID(y[0],y[1]);
                tp=labels.get(id).textProperty();
                try
                {
                    // if(timeDayToID(IDtoStartTime(id),IDtoDay(id)).equals(id))
                    // {
                    tp.setValue(y[2]);
                    // }
                    // else
                    // {
                    //     say("woopoo do do doobity do puffle puffle cheesecake");
                    //     tp.setValue("DUMPY");
                    // }
                }
                catch(Exception ignored)
                {
                    //Unimportant: Occurs when  12:30pm◌THURSDAY◌ ⟵ No last thing here. Don't mind me.
                    //java.lang.ArrayIndexOutOfBoundsException: 2
                    // System.out.println("\n\n\n\n\nBORK BORKL: x= "+x);//BORK BORKL: x= 12:30pm◌THURSDAY◌
                    // ignored.printStackTrace();
                    // if(!tp.getValue().equals(""))
                    //     say("IS THIS WHEN IM DELETING THINGS");
                    //
                    // tp.setValue("DOOPY");
                    // say("Bork");
                }
            }
            catch(Exception ignored)
            {
                if(y.length!=2)
                    flag=false;
                    // say("bork");
                //This is what happens if we shrink the office hours. Thi is where it goes if
                // say("Bork bork daddy Bork bork daddy Bork bork daddy Bork bork daddy Bork bork daddy");
            }
        }
        return flag;
    }
    public void clearGridStateExceptShape()
    {
        HashMap<String,Label> labels=getWorkspace().getOfficeHoursGridTACellLabels();
        for(Label c : labels.values())
        {
            try
            {
                c.textProperty().setValue("");
            }
            catch(Exception ignored)
            {
            }
        }
    }
    //endregion
    public void handleRemoveTA(String taName)//Returns the string needed to restore the TA with their office hours.
    {
        getTAData().removeTA(taName);
        // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
        HashMap<String,Label> labels=getWorkspace().getOfficeHoursGridTACellLabels();
        for(Label label : labels.values())
        {
            // String id=label.getId();
            // System.out.println("CHECK: "+id+"    vs    "+timeDayToID(IDtoStartTime(id),IDtoDay(id)));
            // System.out.println("―――――――――――――――――――――――――――――――――――――――――");
//             labels.keySet().iterator().forEachRemaining(System.out::println);
//             System.out.println("DOOBLYDOO: "+label.getId());
            if(label.getText().equals(taName)
               ||(label.getText().contains(taName+"\n"))
               ||(label.getText().contains("\n"+taName)))
            {
                getTAData().removeTAFromCell(label.textProperty(),taName);
            }
        }
        // WE'VE CHANGED STUFF
        markWorkAsEdited();
    }
    //region Clear Button Stuff
    public void handleClearButton()
    {
        //TODO Implement this function after we have undo/redo.
        say("Please implement me");
        clearTaSelection();
        clearTextFields();
        setAddOrUpdateButtonMode(true);//Set the button to 'Add Ta' mode, regardless of its' current mode.
        disableClearButton();
        disable_AddⳆUpdateButton();
    }
    public void clearTextFields()
    {
        // //region Clear the visible GUI strings in the name/email text fields
        getWorkspace().getEmailTextField().textProperty().setValue("");
        getWorkspace().getNameTextField().textProperty().setValue("");
        // //endregion
    }
    String addText=PropertiesManager.getPropertiesManager().getProperty(TAManagerProp.ADD_BUTTON_TEXT);
    String updateText=PropertiesManager.getPropertiesManager().getProperty(TAManagerProp.UPDATE_BUTTON_TEXT);
    public boolean getAddOrUpdateButtonMode()//Determined ONLY by the visible text currently on the button
    {
        String buttonText=getWorkspace().addButton.textProperty().getValue();
        // say(buttonText);
        // ≣ String _b_="Add TA",updateText="Update TA";
        assert buttonText.equals(addText)||buttonText.equals(updateText);//Nothing outside of this method should be able to set it.
        return buttonText.equals(addText);
    }
    public void setAddOrUpdateButtonMode(boolean addIfTrue_falseIfUpdate)
    {
        getWorkspace().addButton.textProperty().set(addIfTrue_falseIfUpdate?addText:updateText);
    }
    // public void toggleTheTextFrom_AddTA_to_UpdateTA()//This method is useless in the final product. It exists for debugging purposes and can be removed if it doesn't cause any problems to do so.
    // {
    //     setAddOrUpdateButtonMode(!getAddOrUpdateButtonMode());
    // }
    //region Enabling/Disabling add/clear buttons
    public void disableClearButton()
    {
        getWorkspace().clearButton.setDisable(true);
    }
    public void enableClearButton()
    {
        getWorkspace().clearButton.setDisable(false);
    }
    public void disable_AddⳆUpdateButton()
    {
        getWorkspace().addButton.setDisable(true);
    }
    public void enable_AddⳆUpdateButton()
    {
        getWorkspace().addButton.setDisable(false);
    }
    //endregion
    public void handleNameTextFieldChanged()
    {
        handleEitherEmailOrNameTextFieldChanged();
    }
    public void handleEmailTextFieldChanged()
    {
        handleEitherEmailOrNameTextFieldChanged();
    }
    public void handleEitherEmailOrNameTextFieldChanged()
    {
        enableClearButton();
        updateWhetherAddⳆUpdateButtonIsEnabledDependingOnIfLegitNameAndEmailAreEntered();
    }
    public void updateWhetherAddⳆUpdateButtonIsEnabledDependingOnIfLegitNameAndEmailAreEntered()
    {
        if(isLegitNameAndEmailCombo(getNameTextFieldValue(),getEmailTextFieldValue(),false))
        {
            enable_AddⳆUpdateButton();
        }
        else
        {
            disable_AddⳆUpdateButton();
        }
    }
    public String getEmailTextFieldValue()
    {
        return getWorkspace().getEmailTextField().textProperty().getValue();
    }
    public String getNameTextFieldValue()
    {
        return getWorkspace().getNameTextField().textProperty().getValue();
    }
    public void clearTaSelection()
    {
        getWorkspace().getTATable().getSelectionModel().clearSelection();
    }
    public void handleTASelected()//Triggered when a new TA is selected, regardless of how (whether it be by mouse or keyboard etc)
    {
        if(getSelectedTA()!=null)//Only do stuff if we selected a new ta.
        {
            say("wowza maybe we can updates");
            setAddOrUpdateButtonMode(false);
            getWorkspace().getEmailTextField().textProperty().setValue(getSelectedTA().getEmail());
            getWorkspace().getNameTextField().textProperty().setValue(getSelectedTA().getName());
        }
    }
    //endregion
    //endregion
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(TAManagerApp initApp)
    {
        // KEEP THIS FOR LATER
        app=initApp;
        app.controller=this;
    }
    /**
     * This helper method should be called every time an edit happens.
     */
    public void markWorkAsEdited()
    {
        // MARK WORK AS EDITED
        AppGUI gui=app.getGUI();
        gui.fileController.markAsEdited();
        // gui.fileController.saved=false; //⟵ This caused problems. I don't know why.
        // gui.updateToolbarControls(false);
    }
    @SuppressWarnings("Duplicates")
    public void handleAddⳆUpdateTA()
    {
        // say("jesus");
        TextField nameTextField=getWorkspace().getNameTextField();
        TextField emailTextField=getWorkspace().getEmailTextField();
        final String newName=nameTextField.getText();
        final String newEmail=emailTextField.getText();
        if(isLegitNameAndEmailCombo(newName,newEmail,true))
        {
            // sleep(3);
            // say("jesus");
            // sleep(3);
            if(getAddOrUpdateButtonMode())//Add Mode
            {
                // say("cracker");
                app.urc.Do(()->addTa(newName,newEmail),()->handleRemoveTA(newName));
            }
            else//Update TA mode
            {
                //PLEASE NOTE: You can break my code with this edge case: If one person's name fully contains another person's name. Though, I think this will be EXTREMELY unlikely so I'm going to ignore that possibility for the sake of sanity.
                final String oldName=getSelectedTA().getName();
                final String oldEmail=getSelectedTA().getEmail();
                final String oldState=getGridStateIncludingSize();
                // say("greeeeeeen");
                final String newState=oldState.replaceAll(Pattern.quote(oldName),newName).replaceAll(Pattern.quote(oldEmail),newEmail);
                // say("baaaaaaaarbie world is a dinosaur and stuff");
                app.urc.Do(()->
                           {
                               getTAData().removeTA(oldName);
                               getTAData().addTA(newName,newEmail);
                               setGridStateIncludingSize(newState);
                           },()->
                           {
                               getTAData().removeTA(newName);
                               getTAData().addTA(oldName,oldEmail);
                               setGridStateIncludingSize(oldState);
                           });
                handleClearButton();
            }
        }

    }
    boolean isLegitNameAndEmailCombo(String name,String email,boolean showDialogs)
    {
        /*@formatter:off*/
        if(name.isEmpty())// DID THE USER NEGLECT TO PROVIDE A TA NAME?
        {
            if(showDialogs)showDialog(MISSING_TA_NAME_TITLE,MISSING_TA_NAME_MESSAGE);
            return false;
        }
        else if(email.isEmpty())// DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        {
            if(showDialogs)showDialog(MISSING_TA_EMAIL_TITLE,MISSING_TA_EMAIL_MESSAGE);
            return false;
        }
        else if(getTAData().containsTAWithEither(name,email)&&getAddOrUpdateButtonMode()||/*//Is add mode*/!getAddOrUpdateButtonMode()&&/*//Is update mode*/getTAData().containsTAWithBoth(name,email))//PLEASE NOTE: You can break this check with special cases. This night cause disaster. But i doubt anybody would ever do that.
        {
            if(showDialogs)showDialog(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE,TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE);
            return false;
        }
        else if(!isValidEmail(email))// Ryan: Is the email invalid?
        {
            if(showDialogs)showDialog(BAD_EMAIL_TITLE,BAD_EMAIL_MESSAGE);
            return false;
        }
        return true;
        /*@formatter:on*/
    }
    /**
     * This method responds to when the user requests to add
     * _a_ new TA via the UI. Note that it must first do some
     * validation to make sure _a_ unique name and email address
     * has been provided.
     */
    public void addTa(String name,String email)
    {
        //region If all goes well and we have valid Email and Name:
        if(isLegitNameAndEmailCombo(name,email,false))// EVERYTHING IS FINE, ADD A NEW TA
        {
            getTAData().addTA(name,email);// ADD THE NEW TA TO THE DATA
            getWorkspace().getNameTextField().setText("");// CLEAR THE TEXT FIELDS
            getWorkspace().getEmailTextField().setText("");// ...DITTO...
            getWorkspace().getNameTextField().requestFocus();// AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            markWorkAsEdited();// WE'VE CHANGED STUFF
        }
        else
        {
            say("ya phukked up mate. bad name and email combo. this shouldn't be possible to reach this line so imma throw a false assertion now.");
            assert false;
        }
        /*@formatter:on*/
        //endregion
    }
    /**
     * This function provides _a_ response for when the user presses _a_
     * keyboard key. Note that we're only responding to Delete, to remove
     * _a_ TA.
     *
     * @param code The keyboard code pressed.
     */
    public void handleKeyPress(KeyCode code)
    {
        if(code==KeyCode.DELETE||code==KeyCode.BACK_SPACE)// DID THE USER PRESS THE DELETE KEY? //Mac-Friendly
        {
            if(getSelectedTA()!=null)// IS A TA SELECTED IN THE TABLE?
            {
                final String currentstate=getGridStateIncludingSize();
                final String taName=getSelectedTA().getName();
                final String taEmail=getSelectedTA().getEmail();
                app.urc.Do(()->handleRemoveTA(taName),()->
                {
                    //Simul ⦂
                    setGridStateIncludingSize(currentstate);
                    assert isLegitNameAndEmailCombo(taName,taEmail,false);
                    addTa(taName,taEmail);
                });// GET THE TA AND REMOVE IT
            }
        }
    }
    public String teststate=null;
    public void handleKeyPressTheRyanWay(KeyEvent ke)//This receives key events globally; from all panes in the entire program.
    {
        //FROM http://stackoverflow.com/questions/5970765/java-detect-ctrlx-key-combination-on-a-jtree
        /*@formatter:off*/
        if(ke.isControlDown()||ke.isMetaDown())// If ⌃ or ⌘
            if(ke.getCode()==KeyCode.Z)
                app.urc.Undo();
            else if(ke.getCode()==KeyCode.Y)
                app.urc.Redo();
        /*@formatter:on*/
        if(ke.getCode()==KeyCode.S)
        {
            say("Saving grid state including size");
            teststate=getGridStateIncludingSize();
        }
        if(ke.getCode()==KeyCode.L)
        {
            say("Loading grid state including size");
            setGridStateIncludingSize(teststate);
        }
        if(ke.getCode()==KeyCode.R&&(ke.isControlDown()||ke.isMetaDown()))
        {
            say("Getting inputted");
            int s=Integer.parseInt(getNameTextFieldValue());
            int e=Integer.parseInt(getEmailTextFieldValue());
            say("Start time is "+s+" and end time is "+e);
            getWorkspace().setStartAndEndHours(s,e);
        }
    }
    /**
     * This function provides _a_ response for when the user clicks
     * on the office hours grid to add or remove _a_ TA to _a_ time slot.
     *
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane)
    {
        // GET THE TABLE
        final TAWorkspace workspace=(TAWorkspace)app.getWorkspaceComponent();
        final TableView taTable=workspace.getTATable();
        // IS A TA SELECTED IN THE TABLE?
        final Object selectedItem=taTable.getSelectionModel().getSelectedItem();
        if(selectedItem!=null)
        {
            // GET THE TA
            TeachingAssistant ta=(TeachingAssistant)selectedItem;
            String taName=ta.getName();
            final TAData data=(TAData)app.getDataComponent();
            final String cellKey=pane.getId();
            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
//⁠⁠⁠⁠⁠                    ⎧                                                                                         ⎫
//⁠⁠⁠⁠⁠                    ⎪⎧⎫                          ⎧              ⎫ ⎧⎫                          ⎧              ⎫⎪
            app.urc.Do(()->data.toggleTAOfficeHours(cellKey,taName),()->data.toggleTAOfficeHours(cellKey,taName));
//⁠⁠⁠⁠⁠                    ⎪⎩⎭                          ⎩              ⎭ ⎩⎭                          ⎩              ⎭⎪
//⁠⁠⁠⁠⁠                    ⎩                                                                                         ⎭
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }
    void handleGridCellMouseExited(Pane pane)
    {
        String cellKey=pane.getId();
        TAData data=getTAData();
        int column=Integer.parseInt(cellKey.substring(0,cellKey.indexOf("_")));
        int row=Integer.parseInt(cellKey.substring(cellKey.indexOf("_")+1));
        TAWorkspace workspace=getWorkspace();
        Pane mousedOverPane=workspace.getTACellPane(data.getCellKey(column,row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane=workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column,0));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        // THE MOUSED OVER ROW HEADERS
        headerPane=workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0,row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane=workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1,row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for(int i=2;i<column;i++)
        {
            cellKey=data.getCellKey(i,row);
            Pane cell=workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for(int i=1;i<row;i++)
        {
            cellKey=data.getCellKey(column,i);
            Pane cell=workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
    }
    void handleGridCellMouseEntered(Pane pane)
    {
        String cellKey=pane.getId();
        TAData data=getTAData();
        int column=Integer.parseInt(cellKey.substring(0,cellKey.indexOf("_")));
        int row=Integer.parseInt(cellKey.substring(cellKey.indexOf("_")+1));
        TAWorkspace workspace=getWorkspace();
        // THE MOUSED OVER PANE
        Pane mousedOverPane=workspace.getTACellPane(data.getCellKey(column,row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_CELL);
        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane=workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column,0));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        // THE MOUSED OVER ROW HEADERS
        headerPane=workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0,row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane=workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1,row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for(int i=2;i<column;i++)
        {
            cellKey=data.getCellKey(i,row);
            Pane cell=workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }
        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for(int i=1;i<row;i++)
        {
            cellKey=data.getCellKey(column,i);
            Pane cell=workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }
    }
}